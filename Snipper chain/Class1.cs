using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Class1
    {
        static void Main()
        {
            int ant = Convert.ToInt32(Console.ReadLine());
            int[] dp = new int[10001];
            dp[0] = 0;
            dp[1] = 1;
            dp[2] = 3;
            dp[3] = 7;
            for (int i = 4; i < 10001; i++)
            {
                dp[i] = (dp[i - 1] * 2) + 1;
            }
            for (int i = 0; i < ant; i++)
            {
                String [] inn = Console.ReadLine().Split(' ');
                long stikk = Convert.ToInt64(inn[0]);
                long knips = Convert.ToInt64(inn[1]);
                if (knips == dp[stikk] || (knips-dp[stikk])%(dp[stikk]+1)==0)
                {
                    Console.WriteLine("Case #" + (1 + i) + ": ON");
                }
                else
                    Console.WriteLine("Case #" + (1 + i) + ": OFF");



            }
        }
            
    }
}
