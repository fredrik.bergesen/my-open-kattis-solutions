import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 */
public class Paintball {
    public static int [] m;
    public static boolean b [];
    public static void main (String [] args)
    {
        Kattio io = new Kattio(System.in);
        int ant = io.getInt();
        int c = io.getInt();
       ArrayList<ArrayList<Integer>> g = new ArrayList<>();
        for (int i = 0; i < ant; i++) {
            g.add(new ArrayList<>());
        }
        for (int i = 0; i < c; i++) {
            int en = io.getInt();
            int to = io.getInt();
            en--;
            to--;
            g.get(en).add(to);
            g.get(to).add(en);
        }
        m = new int[ant];
        Arrays.fill(m,-1);
        int t = match(g, ant);
        if (t< ant)
        {
            System.out.println("Impossible");
        }
        else
        {
            for (int i = 0; i < m.length; i++) {
                System.out.println((m[i]+1));
            }
        }


    }
    static int match (ArrayList<ArrayList<Integer>> g, int a)
    {
        int teller = 0;
        for (int i = 0; i < a; i++) {
            b = new boolean[a];
            for (int j = 0; j < g.get(i).size(); j++) {
                if (sti(g, g.get(i).get(j)))
                {
                    teller++;
                    m[g.get(i).get(j)] = i;
                    break;
                }
            }
        }
        return teller;

    }
    static boolean sti (ArrayList<ArrayList<Integer>> g, int s)
    {
        if (m[s] == -1)
            return true;
        b [s] = true;
        int mid =  m[s];
        for (int i = 0; i < g.get(mid).size(); i++) {
            int n = g.get(mid).get(i);
            if (!b[n] && sti(g,n))
            {
                m[n] = mid;
                m[s] = -1;
                return true;
            }
        }
        return false;
    }
}
