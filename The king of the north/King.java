import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Queue;

public class King {
    public static int [] sti;
    public static int teller;
    public static void main (String [] args)
    {
        teller = 1;
        Kattio io = new Kattio(System.in);
        int x = io.getInt();
        int y = io.getInt();
        ArrayList <P5> start = new ArrayList<>();
        ArrayList<ArrayList<P5>> map = new ArrayList<>();
        for (int i = 0; i < x; i++) {

            for (int j = 0; j < y; j++) {
                map.add(new ArrayList<>());
                map.add(new ArrayList<>());
            }
        }
        int t = 0;
        map.add(new ArrayList<>());
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                int inn = io.getInt();
                boolean edge = false;
                int teller = (2*y*i)+(j*2);
                map.get(teller).add(new P5(teller+1, inn));
                map.get(teller +1).add(new P5 (teller, 0));

                if (i > 0)
                {
                    map.get(teller+1).add(new P5 (teller -(y*2), Integer.MAX_VALUE));
                    map.get(teller - (y*2)).add(new P5(teller+1,0));
                }
                else
                {
                    edge = true;
                    map.get(map.size()-1).add(new P5 (teller, Integer.MAX_VALUE));
                    map.get(teller).add(new P5 (map.size()-1, 0));
                    start.add(new P5 (teller, Integer.MAX_VALUE));
                }
                if (i < x-1)
                {
                    map.get(teller+1).add(new P5 (teller +(y*2), Integer.MAX_VALUE));
                    map.get(teller + (y*2)).add(new P5(teller+1,0));
                }
                else if (!edge)
                {
                    edge = true;
                    start.add(new P5 (teller, Integer.MAX_VALUE));
                    map.get(map.size()-1).add(new P5 (teller, Integer.MAX_VALUE));
                    map.get(teller).add(new P5 (map.size()-1, 0));
                }
                if (j >0)
                {
                    map.get(teller+1).add(new P5 (teller -2, Integer.MAX_VALUE));
                    map.get(teller - 2).add(new P5(teller+1,0));
                }
                else if (!edge)
                {
                    edge = true;
                    start.add(new P5 (teller, Integer.MAX_VALUE));
                    map.get(map.size()-1).add(new P5 (teller, Integer.MAX_VALUE));
                    map.get(teller).add(new P5 (map.size()-1, 0));
                }
                if (j < y-1)
                {
                    map.get(teller+1).add(new P5 (teller +2, Integer.MAX_VALUE));
                    map.get(teller + 2).add(new P5(teller+1,0));
                }
                else if (!edge)
                {
                    edge = true;
                    start.add(new P5 (teller, Integer.MAX_VALUE));
                    map.get(map.size()-1).add(new P5 (teller, Integer.MAX_VALUE));
                    map.get(teller).add(new P5 (map.size()-1, 0));
                }

            }
        }
        int st = ((2*y*io.getInt())+(io.getInt()*2))+1;

        int ut = 0;


            while (finnSti(map.size()-1, map, st)) {
                int flaskehals = Integer.MAX_VALUE;
                int forje = st;
                for (int i = st; i < map.size()-1; i = sti[i]) {
                }
                for (int i = sti[st]; i != map.size()-1; i = sti[i]) {
                    int m = 0;
                    for (int j = 0; j < map.get(i).size(); j++) {
                        if (map.get(i).get(j).to == forje)
                            m = map.get(i).get(j).flow;
                    }

                    flaskehals = Math.min(flaskehals, m);
                    forje = i;

                }

                forje = st;
                int no = 0;
                for (int i = sti[st]; i != map.size()-1; i = sti[i]) {
                    for (int j = 0; j < map.get(i).size(); j++) {
                        if (map.get(i).get(j).to == forje)
                            map.get(i).get(j).flow -= flaskehals;
                    }
                    for (int j = 0; j < map.get(forje).size(); j++) {
                        if (map.get(forje).get(j).to == i)
                            map.get(forje).get(j).flow += flaskehals;
                    }
                    forje = i;
                    no = sti[i];


                }
                ut += flaskehals;
            }

        System.out.println(ut);

    }
    public static boolean finnSti (int s, ArrayList<ArrayList<P5>> map, int slutt)
    {

        ArrayDeque<Integer> q = new ArrayDeque<>();


        boolean besøkt [] =  new boolean[map.size()];
        sti = new int[map.size()];
        q.add(s);
        besøkt[s] = true;
        while (!q.isEmpty() && !besøkt[slutt])
        {
            int mid = q.poll();
            for (P5 temp : map.get(mid)) {
                if (temp.flow > 0 && !besøkt[temp.to])
                {
                    sti [temp.to] = mid;
                    besøkt[temp.to] = true;
                    q.add(temp.to);

                }
            }
        }
        return besøkt [slutt];
    }
}
class P5 {
    int to;
    int flow;
    public P5 (int t, int f) {
        to = t;
        flow = f;
    }
}
