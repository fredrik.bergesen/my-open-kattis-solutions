/**
 * regner det egentlig bare ut ved hjelp at pytagoras
 * linjær tid
 */
public class ImpGPS {
    public static void main (String [] args)
    {
        Kattio io = new Kattio(System.in);
        int inn = io.getInt();
        int interval = io.getInt();
        int [][] map = new int[inn][3];
        map[0][0] = io.getInt();
        map[0][1] = io.getInt();
        map[0][2] = io.getInt();
        double rettSumm = 0;
        for (int i = 1; i < inn; i++) {
            map[i][0] = io.getInt();
            map[i][1] = io.getInt();
            map[i][2] = io.getInt();
            rettSumm += Math.sqrt(Math.pow(((double)map[i-1][0] - (double)map[i][0]), 2) + Math.pow(((double)map[i-1][1] - (double)map[i][1]), 2));
        }
        int slutttid = map[inn-1][2];
        int teller = 1;
        int tid = interval;
        double sum = 0;
        double forjeX = (double) map[0][0];
        double forjeY = (double)map[0][1];
        while (tid < slutttid)
        {
            if (tid > map[teller][2])
                teller++;
            else if (tid == map[teller][2])
            {
                sum += Math.sqrt(Math.pow((forjeX - (double)map[teller][0]), 2) + Math.pow((forjeY - (double)map[teller][1]), 2));
                forjeX = (double) map [teller][0];
                forjeY = (double) map [teller][1];
                tid += interval;
                teller++;
            }
            else
            {
                int tidDiff1 = tid - map[teller-1][2];
                int tidDiff2 = map[teller][2] - map [teller-1][2];
                double nyX = ((((double)map[teller][0] - (double)map[teller-1][0])/tidDiff2)*tidDiff1) + (double)map[teller-1][0];
                double nyY = ((((double)map[teller][1] - (double)map[teller-1][1])/tidDiff2)*tidDiff1) + (double)map[teller-1][1];
                sum += Math.sqrt(Math.pow((forjeX - nyX), 2) + Math.pow((forjeY - nyY), 2));
                forjeX = nyX;
                forjeY = nyY;
                tid += interval;
            }
        }
        sum += Math.sqrt(Math.pow((forjeX - (double)map[inn-1][0]), 2) + Math.pow((forjeY - (double)map[inn-1][1]), 2));
        double dif = Math.abs(rettSumm-sum);
        double ut = (dif/rettSumm)*100;
        System.out.println(ut);
    }
}
