
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class MapColoring {
    public static Random r = new Random();
    public static void main (String [] args)
    {
        Kattio io = new Kattio(System.in);
        int ant = io.getInt();
        for (int l = 0; l < ant; l++) {


            int c = io.getInt();
            int b = io.getInt();
            ArrayList<ArrayList<Integer>> map = new ArrayList<>();
            for (int i = 0; i < c; i++) {
                map.add(new ArrayList<>());
            }
            for (int i = 0; i < b; i++) {
                int en = io.getInt();
                int to = io.getInt();
                map.get(en).add(to);
                map.get(to).add(en);
            }
            if (b < 1)
                System.out.println(1);
            else {
                int min = run(map, c, 2);
                int max = min;
                for (int i = 0; i < 10000; i++) {
                    int no = run(map, c, 2);
                    min = Math.min(min, no);
                }
                if (min > 4)
                    System.out.println("many");
                else
                    System.out.println(min);
            }
        }
    }
    public static int run (ArrayList<ArrayList<Integer>> m, int l, int fager)
    {
        int gf [] = new int[l];
        for (int i = 0; i < l; i++) {
            boolean [] used = new boolean[fager+1];
            ArrayList<Integer> possible = new ArrayList<>();
            for (int u : m.get(i))
            {
                used[gf[u]] = true;
            }
            for (int j = 1; j < used.length; j++) {
                if (!used[j])
                {
                    possible.add(j);
                }
            }
            if (possible.isEmpty())
            {
                return run (m,l,fager+1);
            }
            else if (possible.size() == 1)
            {
                gf[i] = possible.get(0);
            }
            else
            {

                int ra = r.nextInt(possible.size());
                gf[i] = possible.get(ra);
            }
            if (fager >= 5)
                return 5;
        }

        return fager;
    }
}
