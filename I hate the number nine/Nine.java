import java.math.BigInteger;

public class Nine {
    public static void main (String [] args)
    {
        Kattio io = new Kattio(System.in);
        int ant = io.getInt();
        for (int k = 0; k < ant; k++) {
            long tall = io.getLong();

            if (tall == 1)
                System.out.println(8);
            else
            {
                BigInteger ener = new BigInteger("8");
                BigInteger b = new BigInteger("9");
                tall--;
                b = b.modPow(new BigInteger(Long.toString(tall)),new BigInteger("1000000007"));
                b = b.multiply(ener);
                b = b.mod(new BigInteger("1000000007"));
                System.out.println(b.toString());
            }
        }
    }
}
