import java.util.Arrays;

public class Fish {
    public static void main (String [] args)
    {
        Kattio io = new Kattio(System.in);
        int fish = io.getInt();
        int kjopere = io.getInt();
        long [] vekter = new long [fish];
        par8 [] kj = new par8[kjopere];
        for (int i = 0; i < fish; i++) {
            vekter[i] = io.getInt();
        }
        for (int i = 0; i < kjopere; i++) {
            kj[i] = new par8(io.getInt(), io.getInt());
        }
        Arrays.sort(vekter);
        Arrays.sort(kj);
        int tellerF = fish-1;
        int tellerK = kjopere-1;
        long sum = 0;
        //System.out.println(vekter[tellerF]);
        //System.out.println(kj[tellerK].b);
        while (tellerF >= 0 && tellerK >= 0)
        {
            sum += vekter[tellerF]*kj[tellerK].b;
            kj[tellerK].a--;
            if (kj[tellerK].a <= 0)
                tellerK--;
            tellerF--;
        }
        System.out.println(sum);
    }
}
class par8 implements  Comparable<par8>{
    long a;
    long b;
    public par8 (long c, long d)
    {
        a = c;
        b = d;
    }
    @Override
    public int compareTo(par8 p)
    {
        return Long.compare(b, p.b);
    }


}
